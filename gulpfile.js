const gulp         = require('gulp'),
      sass         = require('gulp-sass'),
      browserSync  = require('browser-sync'),
      del          = require('del'),
      image        = require('gulp-image'),
      autoprefixer = require('gulp-autoprefixer'),
      watch        = require('gulp-watch'),
      runSequence  = require('run-sequence'),
      csso         = require('gulp-csso'),
      htmlmin      = require('gulp-htmlmin');


//Clean
gulp.task('clean', function() {
	return del.sync('./build'); // Удаляем папку build перед сборкой
});

  
//Sass | Scss
gulp.task('sass', function () {
	return gulp.src('./source/styles/**/*.+(scss|sass)')   // Источник  // "/**/*" - Выбор всех файлов в папке и подпапках
    	.pipe(sass().on('error', sass.logError))     // Sass в CSS
		.pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true })) // Префиксы
		.pipe(csso())
    	.pipe(gulp.dest('./build/styles/'))  	      // Выгружаем результат в папку source/styles
    	.pipe(browserSync.reload({stream: true}));   // Обновляем CSS на странице при изменении
});


//Html
gulp.task('html', function () {
	gulp.src('./source/pages/index.html')
		.pipe(htmlmin({collapseWhitespace: true}))
		.pipe(gulp.dest('./build/'));
});


//CSS
gulp.task('css', function () {
	gulp.src('./source/styles/**/*.css')
		.pipe(csso())
		.pipe(gulp.dest('./build/styles'));
});


//Scripts
gulp.task('js', function () {
	gulp.src('./source/js/**/*')
		.pipe(gulp.dest('./build/js/'));
});


//Video
gulp.task('video', function () {
	gulp.src('./source/video/**/*')
		.pipe(gulp.dest('./build/video/'));
});


//Image
gulp.task('image', function () {
	gulp.src('./source/img/**/*')
//		.pipe(image())
		.pipe(gulp.dest('./build/img/'));
});


//Fonts
gulp.task('fonts', function () {
	gulp.src('./source/fonts/**/*')
		.pipe(gulp.dest('./build/fonts/'));
});


//Browser-sync
gulp.task('browser-sync', function() { 
	browserSync({
		server: {
			baseDir: './build/'
		},
		notify: false
	});
});


//Watch
gulp.task('watch', function () {
	gulp.watch('./source/styles/**/*.scss', ['sass']).on("change", browserSync.reload);;
	gulp.watch('./source/styles/**/*.css').on("change", browserSync.reload);
	gulp.watch('./source/pages/*.html', ['html']).on("change", browserSync.reload);
	gulp.watch('./source/js/**/*.js', ['js']).on("change", browserSync.reload);
	gulp.watch('./source/img/**/*', ['image']).on("change", browserSync.reload);
	gulp.watch('./source/fonts/**/*', ['fonts']).on("change", browserSync.reload);
});


//Build Output
gulp.task('build', ['clean', 'sass', 'html', 'css', 'js', 'image', 'video', 'fonts']);


//Gulp default
gulp.task('default', ['build', 'browser-sync', 'watch']);